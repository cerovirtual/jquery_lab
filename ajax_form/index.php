<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>AJAX Form</title>
    <link rel="stylesheet" href="../style.css">
    <script src="../jquery-3.1.1.min.js"></script>
    <script>
        $(document).ready(function (){

            $('#spinner').css('display', 'none');

            $('#submit').click(function (e) {
                e.preventDefault();

                var formData = $('#form-measures').serialize();
                $('input').removeClass('error');
                $('#spinner').css('display', 'block');
                $('#result').html('Processing...');

                function processResult(response) {
                    if (response.result) {
                        $('#result').html('The result is: ' + response.result);
                    } else {
                        $.each(response.errors, function (index, value) {
                            $('#result').html('There were errors on the values.');
                            $("#" + index).addClass('error');
                        });
                    }
                }

                $.ajax({
                    url: 'process_measurements.php',
                    method: 'POST',
                    dataType: 'json',
                    data: formData,

                    success: function (response) {
                        processResult(response);
                    },

                    error: function (jhx, options, error) {
                        $('#result').html('An error occurred: ' + error);
                        console.log('An error occurred: ' + error);
                    },

                    complete: function () {
                        $('#spinner').css('display', 'none');
                    }
                });
            });
          
        });
    </script>
</head>
<body>
    <h1>AJAX Form</h1>
    <div id="content">
        <form action="" id ="form-measures">
            <label for="length">Length</label>
            <input type="text" id="length" name="length">
            <br>
            <label for="width">Width</label>
            <input type="text" id="width" name="width">
            <br>
            <label for="height">Height</label>
            <input type="text" id="height" name="height">
            <br><br>
            <button id="submit">Submit</button>
        </form>
    </div>
    <div id="spinner">
        <img src="spinner.gif" width="50" height="50" />
    </div>
    <div id="result">

    </div>
</body>
</html>
