<?php

// simulates slow server
sleep(2);

$errors = [];

if (isset($_POST['length']) && is_numeric($_POST['length'])) {
    $length = $_POST['length'];
} else {
    $errors['length'] = 'Length field invalid.';
}

if (isset($_POST['width']) && is_numeric($_POST['width'])) {
    $width = $_POST['width'];
} else {
    $errors['width'] = 'Width field invalid.';
}

if (isset($_POST['height']) && is_numeric($_POST['height'])) {
    $height = $_POST['height'];
} else {
    $errors['height'] = 'Height field invalid.';
}

if (count($errors) > 0) {
    echo json_encode(['errors' => $errors]);
} else {
    $result = $length * $width * $height;
    echo json_encode(['result' => $result]);
}